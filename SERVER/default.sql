-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Wersja serwera:               5.5.23 - MySQL Community Server (GPL)
-- Serwer OS:                    Win32
-- HeidiSQL Wersja:              8.2.0.4675
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Zrzut struktury tabela digitalburn.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(40) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(42) COLLATE utf8_polish_ci NOT NULL,
  `group_id` int(11) unsigned NOT NULL DEFAULT '2',
  `email` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `last_password_change` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `ranking_points` int(11) DEFAULT '0',
  `ranking_position` int(11) DEFAULT NULL,
  `nick` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `avatar_file` varchar(30) COLLATE utf8_polish_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL,
  `last_sid` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `sid_end` datetime DEFAULT NULL,
  `is_active` tinyint(3) unsigned DEFAULT NULL,
  `is_ban` tinyint(3) unsigned DEFAULT '0',
  `activation_token` varchar(40) COLLATE utf8_polish_ci DEFAULT NULL,
  `last_ip` varchar(24) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `password` (`password`),
  KEY `is_active` (`is_active`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Zrzucanie danych dla tabeli digitalburn.users: ~16 rows (około)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `group_id`, `email`, `created`, `last_password_change`, `last_login`, `modified`, `ranking_points`, `ranking_position`, `nick`, `avatar_file`, `description`, `last_sid`, `sid_end`, `is_active`, `is_ban`, `activation_token`, `last_ip`) VALUES
	(1, 'van2', '659a3a679e9351036409e859c819aba51b9d6e89', 2, NULL, '2013-12-20 07:36:33', NULL, NULL, '2013-12-20 07:36:33', 0, NULL, 'van2', NULL, NULL, NULL, NULL, NULL, 0, '14d99b2fe6b419461c5b1485a0256041df65f7d0', NULL),
	(2, 'test', '659a3a679e9351036409e859c819aba51b9d6e89', 2, NULL, '2013-12-27 14:56:04', NULL, NULL, '2013-12-27 14:56:04', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'da9bba22b251f0ed1b7468c9de79f307c407259a', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
