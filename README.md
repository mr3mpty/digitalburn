Repository structure:

+SERVER
	Server application and stuff related to it (dumps and other)

+GAME
	Client, gfx, unity projects, models

+ADMIN
	Admin django stuff

+UTILS
	Everything what doesn't fit anywhere else