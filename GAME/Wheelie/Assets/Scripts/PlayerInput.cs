﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour {

	// Use this for initialization

	protected CarDriving carDrivingScript;
	protected CarShooting carShootingScript;

	void Start () {
		carDrivingScript = this.GetComponent("CarDriving") as CarDriving;
		carShootingScript = this.GetComponent("CarShooting") as CarShooting;
	}
	
	// Update is called once per frame
	void Update () {
		carDrivingScript.horizonal = Input.GetAxis("Horizontal");
		carDrivingScript.vertical = Input.GetAxis("Vertical");

		if(Input.GetKeyUp(KeyCode.R)){
			carDrivingScript.reset();
		}

		if(Input.GetKeyUp(KeyCode.Space)){
			carShootingScript.Shoot();
		}
	}
}
