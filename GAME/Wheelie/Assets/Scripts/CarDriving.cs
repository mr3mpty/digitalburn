﻿using UnityEngine;
using System.Collections;

public class CarDriving : Photon.MonoBehaviour {

	public WheelCollider frontRightCol;
	public WheelCollider frontLeftCol;

	public WheelCollider rearRightCol;
	public WheelCollider rearLeftCol;

	private float[] gearRatio = {1.88f, 1.41f, 1.13f, 0.95f};
	public int currentGear = 0;

	public float engineTorque = 600;
	public float maxRPM = 6000;
	public float minRPM = 1000;
	public float RPM;

	public float leftRPM;
	public float rightRPM;

	public float horizonal;
	public float vertical;

	public float steerAngle;

	public bool fourwheels = false;
	public bool handbrake = false;

	// Use this for initialization
	void Start () {
		this.rigidbody.centerOfMass = new Vector3(0.0f, -0.5f, 0.0f);
	}

	public void reset(){
		this.transform.position = new Vector3(0,1,0);
		this.transform.rotation = Quaternion.identity;
	}
	
	// Update is called once per frame
	void Update () {
		rigidbody.drag = rigidbody.velocity.magnitude / 250;

		this.RPM = (rearLeftCol.rpm + rearRightCol.rpm) / 2 * gearRatio[currentGear];
		automaticGearbox();
	
		//drive forward
		rearLeftCol.motorTorque = engineTorque / gearRatio[currentGear] * vertical;
		rearRightCol.motorTorque = engineTorque / gearRatio[currentGear] * vertical;

		//turn left / right
		//should be * deltaTime
		frontLeftCol.steerAngle = 35 * horizonal;
		frontRightCol.steerAngle = 35 * horizonal;

		steerAngle = frontLeftCol.steerAngle;
	}

	void automaticGearbox(){


		if(RPM >= maxRPM){
			int approperiateGear = currentGear;
			for ( int i = 0; i < gearRatio.Length; i ++ ) {
				if ( rearLeftCol.rpm * gearRatio[i] < maxRPM ) {
					approperiateGear = i;
					break;
				}
			}

			currentGear = approperiateGear;
		}

		if(RPM <= minRPM){
			int approperiateGear = currentGear;
			for ( int j = gearRatio.Length-1; j >= 0; j -- ) {
				if ( rearLeftCol.rpm * gearRatio[j] > minRPM ) {
					approperiateGear = j;
					break;
				}
			}
			
			currentGear = approperiateGear;
		}
	}
}
