using UnityEngine;
using System.Collections;
using System;
//using System.Security.Cryptography.RSACryptoServiceProvider;
using System.Security.Cryptography;
using SimpleJSON;

public class RequestHandlerScript : MonoBehaviour {
	
	protected string wwwResponse;
	public JSONNode ASDoutput;
	private GuiScript MainParent;
	private bool isDone;
	void Start () {

	}

	public void createRequest(string action, ArrayList data, Action<JSONNode> actionToExecute) { //FormField[] data
		string website = "http://localhost:8080/";
		string url = website+"/rest/action";
		if (action == "login") {
			url = website+"/users/login";
		} else if (action == "register") {
			url = website+"/users/register";
		}
		isDone = false;
		WWWForm form = new WWWForm();

		foreach(FormField singleField in data) {
			form.AddField(singleField.getName(), singleField.getValue());
		}
		form.AddField("action",action);
		
		Debug.Log("WWW  : " + url);
		WWW www = new WWW(url, form);
		
		StartCoroutine(WaitForRequest(www,actionToExecute));
		//while(!isDone){
			//Debug.Log("WWW  : Derp Derp derp"+www.isDone);
		//}
		Debug.Log("WWW  : Derp Derp derp"+www.isDone);
		//return ASDoutput;
	}
	
	IEnumerator WaitForRequest(WWW www, Action<JSONNode> actionToExecute)
	{
		yield return www;

			// check for errors
			if (www.error == null)
		{
			wwwResponse = www.data;//www.data;
			
			Debug.Log("WWW response : " + www.text );
			JSONNode output = JSON.Parse(wwwResponse);
			ASDoutput = output;
			Debug.Log("WWW response : " + output["result"]);
			actionToExecute(output);
			//MainParent.costam();

		} else {
			Debug.Log("WWW error : " + www.error );
			wwwResponse = null;
		}    
	}    

	string EncodeClientMessage() {
		try
		{       
			byte[] PublicKey = {77,73,71,102,77,65,48,71,67,83,113,71,83,73,98,51,68,81,69,66,65,81,
				85,65,65,52,71,78,65,68,67,66,105,81,75,66,103,81,68,65,102,103,104,87,101,121,106,56,
				89,73,103,109,110,112,81,105,121,119,85,117,71,68,111,78,13,10,105,48,111,84,43,114,
				106,104,101,100,104,65,100,101,66,105,119,82,112,83,89,113,100,77,116,69,106,72,115,
				67,115,116,74,107,106,80,68,52,115,85,51,47,101,84,81,103,101,48,110,73,121,76,83,69,
				99,72,87,105,73,88,52,118,104,74,13,10,118,105,69,55,105,117,110,113,84,105,102,114,
				116,67,74,87,90,80,108,77,48,109,99,68,107,83,70,86,99,49,119,119,47,75,116,69,97,113,
				119,66,100,54,109,106,48,120,49,76,118,77,100,76,66,52,68,97,49,102,84,48,113,77,72,
				121,13,10,73,97,52,122,69,81,57,86,57,89,100,99,67,57,72,87,67,119,73,68,65,81,65,66};
			
			//Values to store encrypted symmetric keys.
			byte[] EncryptedSymmetricKey;
			byte[] EncryptedSymmetricIV;
			
			//Create a new instance of RSACryptoServiceProvider.
			RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
			
			//Get an instance of RSAParameters from ExportParameters function.
			RSAParameters RSAKeyInfo = RSA.ExportParameters(false);
			
			//Set RSAKeyInfo to the public key values. 
			RSAKeyInfo.Modulus = PublicKey;
			//Import key parameters into RSA.
			RSA.ImportParameters(RSAKeyInfo);
			
			//Create a new instance of the RijndaelManaged class.
			RijndaelManaged RM = new RijndaelManaged();
			
			//Encrypt the symmetric key and IV.
			EncryptedSymmetricKey = RSA.Encrypt(RM.Key, false);
			EncryptedSymmetricIV = RSA.Encrypt(RM.IV, false);
			
			Debug.Log("RijndaelManaged Key and IV have been encrypted with RSACryptoServiceProvider.");
		}
		catch (CryptographicException e)
		{
			Debug.Log(e.Message);
		}
		return "asdfg";
	}

	public static string Base64Encode(string plainText) {
		var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
		return System.Convert.ToBase64String(plainTextBytes);
	}

	
	public static string Base64Decode(string base64EncodedData) {
		var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
		return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
	}
}


