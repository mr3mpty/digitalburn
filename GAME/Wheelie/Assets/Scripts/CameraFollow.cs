﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform target = null;
	public float distance = 3.0f;
	public float height = 3.0f;
	public float damping = 5.0f;
	public bool smoothRotation = true;
	public bool followBehind = true;
	public float rotationDamping = 10.0f;

	// Use this for initialization
	void Start()
	{
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(target != null)
		{
			Vector3 wantedPosition;
			if(followBehind)
				wantedPosition = target.TransformPoint(0, height, -distance);
			else
				wantedPosition = target.TransformPoint(0, height, distance);
			
			transform.position = Vector3.Lerp (transform.position, wantedPosition, Time.deltaTime * damping);
			
			if (smoothRotation) {
				Quaternion wantedRotation = Quaternion.LookRotation(target.position - transform.position, target.up);
				transform.rotation = Quaternion.Slerp (transform.rotation, wantedRotation, Time.deltaTime * rotationDamping);
			}
			else transform.LookAt (target, target.up);
		}
	}

	void OnGUI(){
		GameObject[] cars = GameObject.FindGameObjectsWithTag("Car");
		int i = 0;
		foreach(GameObject go in cars){
			i++;
			Vector3 carPos = Camera.main.WorldToScreenPoint(go.transform.position);
			GUI.Label(new Rect((carPos.x - 50), (Screen.height - carPos.y + 10), 100, 50), "Gracz " + i.ToString());
		}
	}
}
