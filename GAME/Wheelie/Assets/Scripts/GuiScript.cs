﻿using UnityEngine;
using System;
using System.Collections;
using SimpleJSON;

public class GuiScript : MonoBehaviour {
	private enum menuStates {
		START, REGISTER, MAIN_MENU, SETTINGS, RANKING, PROFILE, EDIT_PROFILE, STATS
	};
	private menuStates currentState = menuStates.START;
	public static string username = "";
	private string password = "";
	private string[] passwordChange = { "", "", "" };
	private string sessionKey = "";


	public GUISkin customButton;
	public Texture aTexture;
	
	Vector2 scrollPosition = Vector2.zero;
	string[][] scores = new string[0][];

	void updateScores() {
		scores = new string[5][];
		scores[0] = new string[2]{"raz","50"};
		scores[1] = new string[2]{"dwa","30"};
		scores[2] = new string[2]{"trzy","20"};
		scores[3] = new string[2]{"cztery","10"};
		scores[4] = new string[2]{"piec","2"};
	}

	void OnGUI () {
		GUI.skin = customButton;

		if(aTexture){
			float ratio = aTexture.width/aTexture.height;
			int width = (int)(Screen.height * ratio);
			GUI.DrawTexture(
				new Rect(Screen.width /2 - width/2, 0, 
			         width, Screen.height), aTexture, 
	                ScaleMode.StretchToFill, true, 10.0F);
		}

		GUI.BeginGroup (new Rect (Screen.width / 2 - 150, Screen.height / 2 - 130, 300, 200));
		GUI.Box(new Rect(0,0,300,200), "");
		
		switch(currentState){
		case menuStates.START:
			drawStartPage();
			break;
		case menuStates.REGISTER:
			drawRegisterPage();
			break;
		case menuStates.MAIN_MENU:
			drawMainMenuPage();
			break;
		case menuStates.PROFILE:
			drawProfilePage();
			break;
		case menuStates.EDIT_PROFILE:
			drawEditProfilePage();
			break;
		case menuStates.RANKING:
			drawRankingPage();
			break;
		case menuStates.STATS:
			drawStatsPage();
			break;
		}
		
		GUI.EndGroup ();
	}

	private void drawStartPage(){
		GUI.Label(new Rect(30,20,200,20),"Nazwa użytkownika:");
		username = GUI.TextField(new Rect(30, 40, 240, 25), username, 25);
		GUI.Label(new Rect(30,80,200,20),"Hasło:");
		password = GUI.PasswordField(new Rect(30, 100, 240, 25), password, '*', 25);
		
		if(GUI.Button(new Rect(30,150,110,30), "Zaloguj się")) {
			if(username != "" && password != ""){
				ArrayList forms = new ArrayList();
				GameObject go = new GameObject("foo");
				RequestHandlerScript RHSobject = go.AddComponent<RequestHandlerScript>();
				FormField FFUsername = new FormField("username",username);
				FormField FFPassword = new FormField("password",password);
				forms.Add(FFUsername);
				forms.Add(FFPassword);

			//	RHSobject.createRequest("login", forms, login);
				currentState = menuStates.MAIN_MENU;
			}else{
				
			}
		}
		if(GUI.Button(new Rect(160,150,110,30), "Zarejestruj się")) {
			currentState = menuStates.REGISTER;
			username = password = "";
		}
	}
	public void login(JSONNode output) {
		if (Convert.ToBoolean(output["result"])) {
			currentState = menuStates.MAIN_MENU;
			password = "";
			Debug.Log("Login success");
		} else {
			//make som error 
			Debug.Log("Login failure");
		}
		currentState = menuStates.MAIN_MENU;
	}
	
	public void register(JSONNode output) {
		if (Convert.ToBoolean(output["result"])) {
			currentState = menuStates.MAIN_MENU;
			password = "";
			Debug.Log("Registration success");
		} else {
			//make som error 
			Debug.Log("Registration failure ");
		}
		currentState = menuStates.MAIN_MENU;
	}

	void drawRegisterPage ()
	{
		GUI.Label(new Rect(30,20,200,20),"Podaj nazwę użytkownika:");
		username = GUI.TextField(new Rect(30, 40, 240, 25), username, 25);
		GUI.Label(new Rect(30,80,200,20),"Podaj hasło:");
		password = GUI.PasswordField(new Rect(30, 100, 240, 25), password, '*', 25);
		
		if(GUI.Button(new Rect(30,150,110,30), "Zarejestruj się")) {
			
			if(username != "" && password != ""){
				ArrayList forms = new ArrayList();
				GameObject go = new GameObject("foo");
				RequestHandlerScript RHSobject = go.AddComponent<RequestHandlerScript>();
				FormField FFUsername = new FormField("username",username);
				FormField FFPassword = new FormField("password",password);
				forms.Add(FFUsername);
				forms.Add(FFPassword);
				//RHSobject.createRequest("register", forms,register);
				currentState = menuStates.MAIN_MENU;
			}else{
				
			}
		}
		if(GUI.Button(new Rect(160,150,110,30), "Anuluj")) {
			currentState = menuStates.START;
			username = password = "";
		}
	}



	void drawMainMenuPage ()
	{
		if(GUI.Button(new Rect(30,20,240,50), "GRAJ!")) {
			Application.LoadLevel("main");
		}
		
		if(GUI.Button(new Rect(30,80,240,30), "Profil")) {
			currentState = menuStates.PROFILE;
		}
		
		if(GUI.Button(new Rect(30,150,240,30), "Wyloguj się")) {
			currentState = menuStates.START;
		}
	}
	
	void drawProfilePage ()
	{
		if(GUI.Button(new Rect(30,20,240,30), "Edytuj profil")) {
			currentState = menuStates.EDIT_PROFILE;
		}
		
		if(GUI.Button(new Rect(30,60,240,30), "Ranking")) {
			updateScores();
			currentState = menuStates.RANKING;
		}
		
		if(GUI.Button(new Rect(30,100,240,30), "Statystyki")) {
			currentState = menuStates.STATS;
		}
		
		if(GUI.Button(new Rect(30,150,240,30), "Wróć do menu")) {
			currentState = menuStates.MAIN_MENU;
		}
	}
	
	void drawEditProfilePage()
	{
		GUI.Label(new Rect(30,10,200,20),"Stare hasło:");
		passwordChange[0] = GUI.PasswordField(new Rect(30, 30, 240, 25), passwordChange[0], '*', 25);
		GUI.Label(new Rect(30,55,200,20),"Nowe hasło:");
		passwordChange[1] = GUI.PasswordField(new Rect(30, 75, 240, 25), passwordChange[1], '*', 25);
		GUI.Label(new Rect(30,100,200,20),"Powtórz hasło:");
		passwordChange[2] = GUI.PasswordField(new Rect(30, 120, 240, 25), passwordChange[2], '*', 25);
		
		if(GUI.Button(new Rect(30,150,110,30), "Zmień")) {
			if(passwordChange[0] != "" && passwordChange[1] != "" && passwordChange[1] == passwordChange[2]){
				currentState = menuStates.PROFILE;
			}else{
				
			}
		}
		if(GUI.Button(new Rect(160,150,110,30), "Anuluj")) {
			currentState = menuStates.PROFILE;
			
			passwordChange[0] = passwordChange[1] = passwordChange[2] = "";
		}
	}
	
	void drawRankingPage ()
	{
		float win = 240;
		float w1 = win * 0.10f, w2= win * 0.60f, w3= win * 0.20f;
		
		GUI.Label(new Rect(30,5,240,30), "Ranking:");
		
		
		int neededHeight = scores.Length * 33;
		scrollPosition = GUI.BeginScrollView(new Rect(20, 25, 260, 130), scrollPosition, new Rect(0,0,240,neededHeight));
		
		int pos = 1;
		foreach(string[] line in scores){
			GUILayout.BeginHorizontal("Box");
			
			GUILayout.Label(pos.ToString(), GUILayout.Width(w1));
			GUILayout.Label(line[0], GUILayout.Width(w2));
			GUILayout.Label(line[1], GUILayout.Width(w3));
			
			GUILayout.EndHorizontal();
			
			pos++;
		}
		
		GUI.EndScrollView();
		
		if(GUI.Button(new Rect(30,160,240,30), "Wróć do profilu")) {
			currentState = menuStates.PROFILE;
		}
	}
	
	void drawStatsPage ()
	{
		
		if(GUI.Button(new Rect(30,150,240,30), "Wróć do profilu")) {
			currentState = menuStates.PROFILE;
		}
	}
}
