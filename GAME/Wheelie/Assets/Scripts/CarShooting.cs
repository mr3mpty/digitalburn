﻿using UnityEngine;
using System.Collections;

public class CarShooting : MonoBehaviour {

	public Transform bulletPrefab;
	public float bulletSpeed = 1000;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Shoot(){
		Transform clone = (Transform)Instantiate(bulletPrefab, this.transform.position + transform.forward * 3, this.transform.rotation);
		clone.rigidbody.AddRelativeForce(transform.forward * bulletSpeed);

		BulletScript bulletScript = clone.GetComponent("BulletScript") as BulletScript;
		bulletScript.parent = transform;
	}
}
