﻿using UnityEngine;
using System.Collections;

public class Wheel : MonoBehaviour {

	public WheelCollider collider;
	public float rotationValue;

	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		Vector3 coliderCenter = collider.transform.TransformPoint(collider.center);
		if(Physics.Raycast(collider.transform.position, -collider.transform.up, out hit, collider.suspensionDistance + collider.radius)){
			transform.position = hit.point + collider.transform.up * 0.13f;
		} else {
			transform.position = coliderCenter - (collider.transform.up * collider.suspensionDistance);
		}

		transform.rotation = collider.transform.rotation * Quaternion.Euler(0, collider.steerAngle + 90 * Mathf.Sign(transform.rotation.y), rotationValue);
		rotationValue += collider.rpm * (360/60) * Time.deltaTime;
	}
}
