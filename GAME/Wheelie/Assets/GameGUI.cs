﻿using UnityEngine;
using System.Collections;

public class GameGUI : MonoBehaviour {
	
	public GUISkin customButton;
	public int speed;
	public int points;
	public string username = "Gracz";
	public int time;
	
	private float startTime;

	void Start() {
		username = GuiScript.username;
	}

	void Awake() {
		startTime = Time.time;
	}
	
	void OnGUI(){
		GUI.skin = customButton;
		
		float guiTime = Time.time - startTime;
		int minutes = (int)guiTime / 60;
		int seconds = (int)guiTime % 60;
		
		string minStr = minutes < 0 ? "0" : "" + minutes;
		string secStr = seconds < 0 ? "0" : "" + seconds;
		
		//	speed, points
		GUI.BeginGroup (new Rect(10, Screen.height - 80, 100, 100));
		GUI.Label(new Rect(0,0,100,20),"Czas: " + minStr + ":" + secStr);
		GUI.Label(new Rect(0,25,100,20),"Prędkość:" + speed);
		GUI.Label(new Rect(0,50,100,20),"Punkty:" + points);
		GUI.EndGroup ();
		
		GUI.Label(new Rect(Screen.width / 2 - 20, Screen.height / 2 - 150,100,20),username);
	}
}
