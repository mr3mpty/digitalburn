﻿using UnityEngine;
using System.Collections;

public class CarSmoother : Photon.MonoBehaviour {

	private Vector3 correctPlayerPos;
	private Quaternion correctPlayerRot;
	private float horizontal;
	private float vertical;
	private CarDriving carDriving;

	// Use this for initialization
	void Start () {
		this.carDriving = (CarDriving)gameObject.GetComponent<CarDriving>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!photonView.isMine)
		{
			transform.position = Vector3.Lerp(transform.position, this.correctPlayerPos, Time.deltaTime * 5);
			transform.rotation = Quaternion.Lerp(transform.rotation, this.correctPlayerRot, Time.deltaTime * 5);
			this.carDriving.horizonal = horizontal;
			this.carDriving.vertical = vertical;
		}
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		if (stream.isWriting)
		{
			// We own this player: send the others our data
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
			stream.SendNext(carDriving.vertical);
			stream.SendNext(carDriving.horizonal);
			
		}
		else
		{
			// Network player, receive data
			this.correctPlayerPos = (Vector3)stream.ReceiveNext();
			this.correctPlayerRot = (Quaternion)stream.ReceiveNext();
			this.vertical = (float)stream.ReceiveNext();
			this.horizontal = (float)stream.ReceiveNext();
		}
	}
}
