﻿using UnityEngine;
using System.Collections;

public class NetworkManager : Photon.MonoBehaviour {

	public GameObject currentPlayer;
	public Camera useCamera;

	// Use this for initialization
	void Start () {
		PhotonNetwork.ConnectUsingSettings("alpha 1.1");
	}
	
	void OnGUI(){
		GUILayout.Label (PhotonNetwork.connectionStateDetailed.ToString());
	}

	void OnJoinedLobby(){
		PhotonNetwork.JoinRandomRoom();
	}

	void OnPhotonRandomJoinFailed()
	{
		PhotonNetwork.CreateRoom(null);
	}

	void OnJoinedRoom(){
		SpawnPlayer();

		CameraFollow cameraFollow = GameObject.FindWithTag("MainCamera").GetComponent("CameraFollow") as CameraFollow;
		cameraFollow.target = currentPlayer.transform;
	}

	void SpawnPlayer(){
		currentPlayer = PhotonNetwork.Instantiate("Car", new Vector3(0, 0.7f, 0), Quaternion.identity, 0);
		currentPlayer.tag = "Player";
		currentPlayer.name = "Player";
		//currentPlayer.AddComponent<NetworkPlayerSync>();
		currentPlayer.AddComponent<PlayerInput>();
	}
}
