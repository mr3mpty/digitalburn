﻿using UnityEngine;
using System.Collections;

public class GuiScript : MonoBehaviour {
	private enum menuStates {
		START, REGISTER, MAIN_MENU, SETTINGS, RANKING
	};
	private menuStates currentState = menuStates.START;
	private string username = "";
	private string password = "";

	public GUISkin customButton;
	public Texture aTexture;

	void OnGUI () {
		GUI.skin = customButton;

		float scaleX = Screen.width/aTexture.width; // calculate hor scale
		float scaleY = Screen.height/aTexture.height;

		if(aTexture){
			GUI.DrawTexture(
				new Rect(0, 0, 
			         Screen.width, Screen.height), aTexture, 
	                ScaleMode.StretchToFill, true, 10.0F);
		}

		GUI.BeginGroup (new Rect (Screen.width / 2 - 150, Screen.height / 2 - 100, 300, 165));
		GUI.Box(new Rect(0,0,300,165), "");
		
		switch(currentState){
		case menuStates.START:
			drawStartPage();
			break;
		case menuStates.REGISTER:
			drawRegisterPage();
			break;
		case menuStates.MAIN_MENU:
			drawMainMenuPage();
			break;
		case menuStates.SETTINGS:
			break;
		case menuStates.RANKING:
			break;
		}
		
		GUI.EndGroup ();
	}

	private void drawStartPage(){
		GUI.Label(new Rect(30,10,200,20),"Nazwa użytkownika:");
		username = GUI.TextField(new Rect(30, 30, 240, 25), username, 25);
		GUI.Label(new Rect(30,60,200,20),"Hasło:");
		password = GUI.PasswordField(new Rect(30, 80, 240, 25), password, '*', 25);
		
		if(GUI.Button(new Rect(30,120,110,30), "Zaloguj się")) {
			if(username != "" && password != ""){
				currentState = menuStates.MAIN_MENU;
				password = "";
			}else{
				
			}
		}
		if(GUI.Button(new Rect(160,120,110,30), "Zarejestruj się")) {
			currentState = menuStates.REGISTER;
			username = password = "";
		}
	}

	void drawRegisterPage ()
	{
		GUI.Label(new Rect(30,30,200,20),"Podaj nazwę użytkownika:");
		username = GUI.TextField(new Rect(30, 50, 240, 25), username, 25);
		GUI.Label(new Rect(30,90,200,20),"Podaj hasło:");
		password = GUI.PasswordField(new Rect(30, 110, 240, 25), password, '*', 25);
		
		if(GUI.Button(new Rect(30,150,110,30), "Zarejestruj się")) {
			
			if(username != "" && password != ""){
				currentState = menuStates.MAIN_MENU;
				password = "";
			}else{
				
			}
		}
		if(GUI.Button(new Rect(160,150,110,30), "Anuluj")) {
			currentState = menuStates.START;
			username = password = "";
		}
	}

	void drawMainMenuPage ()
	{
		if(GUI.Button(new Rect(30,30,240,30), "Stwórz grę")) {
			//currentState = menuStates.CREATING_GAME;
			//	lub
			//Application.LoadLevel("Game");
		}

		if(GUI.Button(new Rect(30,70,240,30), "Dołącz do gry")) {
			//Application.LoadLevel("Game");
		}

		if(GUI.Button(new Rect(30,110,240,30), "Ustawienia")) {
			//currentState = menuStates.SETTINGS;
		}

		if(GUI.Button(new Rect(30,150,240,30), "Wyloguj się")) {
			currentState = menuStates.START;
		}
	}

}
